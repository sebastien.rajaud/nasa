export const formatDateForAPI = (oldDate) => {

    const newDate = new Date(oldDate);
    const year = newDate.getFullYear();
    const month = formatWithTwoDigits(newDate.getMonth() + 1);
    const day = formatWithTwoDigits(newDate.getDate());
    return `${year}-${month}-${day}`
}

export const formatWithTwoDigits = (number) => {
    return Intl.NumberFormat('fr-FR', {style: 'decimal', minimumIntegerDigits: 2}).format(number)
}

export const formatDateToFrenchFormat = (date) => {
    const dateArray = date.split('-');
    return `${dateArray[2]}/${dateArray[1]}/${dateArray[0]}`

}

export const checkDateAnteriority = (date) => {
    const today = new Date();
    if(date.getTime() > today.getTime()) {
        return false;
    }
    return true
}