import logo from './nasa.svg';
import './scss/component/app.scss';
import Form from './component/Form';
import React, {useState} from 'react';
import Apod from './component/Apod';
import {checkDateAnteriority, formatDateForAPI} from './utils/date';
import Loader from './component/Loader';

const App = () => {

    const [apod, setApod] = useState(null);
    const [isLoading, setIsLoading] = useState(false)
    const [error, setError] = useState(null);

    const getApod = (date) => {

        fetch(`https://api.nasa.gov/planetary/apod?api_key=${process.env.REACT_APP_NASA_KEY}&date=${date}`, {})
            .then(response => response.json())
            .then(data => setApod(data));
    }

    const handleSubmit = (date) => {
        setIsLoading(true)
        const formattedDate = formatDateForAPI(date);
        if (!checkDateAnteriority(date)) {
            setApod(null);
            setIsLoading(false)
            return false;
        }

        getApod(formattedDate);
    }

    return (
        <div className="App">
            <header className="App-header">
                <img src={logo} className="App-logo" alt="logo"/>
                <Form handleSubmit={handleSubmit} handleError={setError}/>
                {apod !== null ? <Apod data={apod}/> : isLoading && <Loader/>}
                {error && <p className="form-error">{error}</p>}
            </header>
        </div>
    );
}

export default App;
