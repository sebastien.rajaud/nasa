import React, {useState} from 'react';
import styles from '../scss/component/apod.module.scss';
import {formatDateToFrenchFormat} from "../utils/date";
import {BsThreeDots} from 'react-icons/bs'
import {IoIosArrowUp} from 'react-icons/io'

const Apod = ({data}) => {

    const [openDesc, setOpenDesc] = useState(false)

    return (
        <div className={styles.apod}>
            {data.hdurl && <div className={styles.apod__img}><img src={data.url} alt=""/></div>}
            <h2 className={styles.apod__title}>{data.title}</h2>
            {data.copyright && <div className={styles.apod__copyright}><h3>Copyright :</h3>{data.copyright}</div>}
            {data.date && <div className={styles.apod__date}><h3>Date :</h3>{formatDateToFrenchFormat(data.date)}</div>}
            {openDesc ? (
                <div className={styles.apod__desc}><h3>Explanation :</h3>
                    <div>{data.explanation}</div>
                    <a href={data.hdurl} target="_blank" rel="noreferrer" className="button">Open media</a><button className="icon-btn" onClick={() => setOpenDesc(!openDesc)}><IoIosArrowUp /></button>
                </div>
            ) : <button className="icon-btn" onClick={() => setOpenDesc(!openDesc)}><BsThreeDots /></button>
            }
        </div>
    )
}

export default Apod;