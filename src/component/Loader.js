import {ImSpinner} from 'react-icons/im';

const Loader = () => {
    return (
        <div className="loader">
            <ImSpinner/>
        </div>
    )
}

export default Loader;