import React, {useState} from 'react';
import DatePicker, {registerLocale} from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";
import styles from '../scss/component/Form.module.scss';
import fr from 'date-fns/locale/fr';
import {checkDateAnteriority} from '../utils/date';

registerLocale('fr', fr)


const Form = ({handleSubmit, handleError}) => {

    const [date, setDate] = useState(null);

    const handleChange = (date) => {
        handleSubmit(date)

        if (!checkDateAnteriority(date)) {
            handleError('La date doit être antérieure à celle d\'aujourd\'hui');
            return false;
        }

        setDate(date)
        handleError(null)
    }
    return (
        <form>
            <DatePicker
                className={styles.datepicker}
                selected={date}
                dateFormat="d/M/Y"
                locale="fr"
                onChange={(date) => handleChange(date)}/>

        </form>
    )
}

export default Form;